package arvore;

import java.io.*;
import java.util.Scanner;

import view.Principal;

public class TreeApp {
	public static void main(String[] args) throws IOException {
		int valor;
		int opcao;
		Arvore yggdrasil = new Arvore(); // Arvore do Programa
		Arvore arvore2 = new Arvore(); // Arvore para comparação de Similaridade
		Arvore arvore3 = new Arvore(); // Arvore para comparação de Igualdade
		Arvore arvore7 = new Arvore(); // Arvore da questão 7

		yggdrasil.inserir(50);
		yggdrasil.inserir(25);
		yggdrasil.inserir(75);
		yggdrasil.inserir(12);
		yggdrasil.inserir(37);
		yggdrasil.inserir(43);
		yggdrasil.inserir(30);
		yggdrasil.inserir(33);
		yggdrasil.inserir(87);
		yggdrasil.inserir(93);
		yggdrasil.inserir(97);

		arvore2.inserir(49);
		arvore2.inserir(24);
		arvore2.inserir(74);
		arvore2.inserir(11);
		arvore2.inserir(36);
		arvore2.inserir(42);
		arvore2.inserir(29);
		arvore2.inserir(32);
		arvore2.inserir(86);
		arvore2.inserir(92);
		arvore2.inserir(96);

		arvore3.inserir(50);
		arvore3.inserir(25);
		arvore3.inserir(75);
		arvore3.inserir(12);
		arvore3.inserir(37);
		arvore3.inserir(43);
		arvore3.inserir(30);
		arvore3.inserir(33);
		arvore3.inserir(87);
		arvore3.inserir(93);
		arvore3.inserir(98);
		
		arvore7.inserir(33);
		arvore7.inserir(15);
		arvore7.inserir(41);
		arvore7.inserir(38);
		arvore7.inserir(34);
		arvore7.inserir(47);
		arvore7.inserir(43);
		arvore7.inserir(49);

		Scanner scan = new Scanner(System.in);
		System.out.println("Escolha o ambiente:");
		System.out.print("[1] - Modo texto\n"
				+ "[2] - Modo interface\n"
				+ "[-1] - Sair\n"
				+ "(#AVISO1 : No modo interface, não é possível fazer comparações entre árvores)\n"
				+ "(#AVISO2 : Após escolher um modo, a única maneira de se retornar, é reiniciar o programa)\n"
				+ "(#AVISO3 : No modo interface, não é possível vizualizar a questão 7)\n"
				+ "->");
		int escolha = Integer.parseInt(scan.nextLine());
		switch (escolha) {
		case 1:
			do {
				System.out.print("Escolha uma opção:\n"
						+ "[1] - Mostrar Árvore\n"
						+ "[2] - Inserir\n"
						+ "[3] - Encontrar\n"
						+ "[4] - Deletar\n" 
						+ "[5] - Atravessar\n" 
						+ "[6] - Contadores\n"
						+ "[7] - Deletar Pares\n" 
						+ "[8] - Comparar Árvores Similares\n"
						+ "[9] - Comparar Árvores Iguais\n"
						+ "[10] - Ordem de visita da questão 7\n"
						+ "[-1] - Sair\n" 
						+ "-> ");
				opcao = getInt();

				switch (opcao) {
				case 1:
					yggdrasil.mostrarArvore();
					break;
				case 2:
					System.out.print("Entre com o valor para inserir: ");
					valor = getInt();
					yggdrasil.inserir(valor);
					break;
				case 3:
					System.out.print("Entre com o valor buscado: ");
					valor = getInt();
					Node encontrado = yggdrasil.buscar(valor);
					if (encontrado != null) {
						System.out.print("Encontrado: ");
						encontrado.displayNode();
						System.out.print("\n");
					} else {
						System.out.print("Valor não encontrado.");
						System.out.print(valor + '\n');
					}
					break;
				case 4:
					System.out.print("Entre com valor que será apagado: ");
					valor = getInt();
					boolean deletado = yggdrasil.delete(valor);
					if (deletado)
						System.out.print("Deletado:  " + valor + '\n');
					else
						System.out.print("Valor não deletado " + valor + '\n');
					break;
				case 5:
					System.out
							.print("Entre com tipo:\n" + "1 - Pré-Ordem\n" + "2 - Ordem\n" + "3 - Pós-Ordem\n" + "-> ");
					valor = getInt();
					yggdrasil.traverse(valor);
					break;
				case 6:
					System.out.print("===================================\n");
					System.out.println("Quantidade de Nós da Árvore: " + yggdrasil.totalNodes());
					System.out.println("Quantidade de Folhas da Árvore: " + yggdrasil.quantFolhas());
					System.out.println("Quantidade de Nós Não-Folha da Árvore: " + yggdrasil.quantNosNaoFolha());
					System.out.println("Altura da árvore: " + yggdrasil.retornaAltura());
					System.out.print("===================================\n");
					break;
				case 7:
					System.out.println("Deletando todos os pares...");
					yggdrasil.deleteAllPares();
					break;
				case 8:
					yggdrasil.mostrarArvore();
					arvore2.mostrarArvore();
					boolean verify = comparaArvoresSimilares(yggdrasil.getRaiz(), arvore2.getRaiz());
					if (verify)
						System.out.println(
								"##############################\n As árvores são similares.\n##############################\n");
					else
						System.out.println(
								"##############################\n As árvores não são similares.\n##############################\n");
					break;
				case 9:
					yggdrasil.mostrarArvore();
					arvore3.mostrarArvore();
					boolean verifyI = comparaArvoresIguais(yggdrasil.getRaiz(), arvore3.getRaiz());
					if (verifyI)
						System.out.println(
								"##############################\n As árvores são iguais.\n##############################\n");
					else
						System.out.println(
								"##############################\n As árvores não são iguais.\n##############################\n");
					break;
				case 10:
					arvore7.mostrarArvore();
					arvore7.traverse(1);
					arvore7.traverse(2);
					arvore7.traverse(3);
					break;
				case -1:
					System.out.println("Programa Encerrado!");
					break;
				default:
					System.out.print("Entrada Inválida!\n");
				}
			} while (opcao != -1);
			break;
		case 2:
			Principal.init();
			break;
		case -1:
			break;
		}
	}

	// -------------------------------------------------------------

	public static String getString() throws IOException {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String s = br.readLine();
		return s;
	}

	// -------------------------------------------------------------
	public static char getChar() throws IOException {
		String s = getString();
		return s.charAt(0);
	}

	// -------------------------------------------------------------
	public static int getInt() throws IOException {
		String s = getString();
		return Integer.parseInt(s);
	}

	// -------------------------------------------------------------

	public static boolean comparaArvoresSimilares(Node raiz1, Node raiz2) {
		if (raiz1 == null && raiz2 == null)
			return true;
		return ((raiz1 != null && raiz2 != null) && (comparaArvoresSimilares(raiz1.direita, raiz2.direita)
				&& comparaArvoresSimilares(raiz1.esquerda, raiz2.esquerda)));
	}

	public static boolean comparaArvoresIguais(Node raiz1, Node raiz2) {
		if (raiz1 == null && raiz2 == null)
			return true;
		return ((raiz1 != null && raiz2 != null) && (raiz1.dado == raiz2.dado)
				&& (comparaArvoresIguais(raiz1.direita, raiz2.direita)
						&& comparaArvoresIguais(raiz1.esquerda, raiz2.esquerda)));
	}
}
